<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends Model
{
    public $name;
    public $phone;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['name', 'phone'], 'required'],
            // email has to be a valid email address
            [['name', 'phone'], 'string'],
            // verifyCode needs to be entered correctly
//            ['verifyCode', 'captcha'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'verifyCode' => 'Verification Code',
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @param string $email the target email address
     * @return bool whether the model passes validation
     */
    public function contact($email)
    {
        $subject = "Новые заявка на ".Yii::$app->request->hostName." — ".date('d.M.Y');

        if ($this->validate()) {
            Yii::$app->mailer->compose('contact',[
                'name' => $this->name,
                'phone' => $this->phone,
            ])
                ->setTo($email)
                ->setFrom(['contact@balzam-korotkov.com' => 'balzam-korotkov.com'])
                ->setSubject($subject)
                ->send();

            return true;
        }
        return false;
    }
}
