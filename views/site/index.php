<?php

/* @var $this yii\web\View */
/* @var $products array */

$this->title = Yii::$app->name;
?>

<!-- Navigation -->
<nav class="navbar navbar-expand-lg bg-secondary text-uppercase fixed-top" id="mainNav">
    <div class="container">

        <div class="navbar-brand js-scroll-trigger" >
            <a class="btn btn-social mx-1" href="https://wa.me/<?=Yii::$app->params['whatsapp'];?>" target="_blank">
                <i class="fab fa-7x fa-whatsapp"></i>
            </a>
            <a class="btn btn-primary btn-social mx-1" href="https://t.me/<?=Yii::$app->params['telegram'];?>" target="_blank">
                <i class="fab fa-fw fa-telegram-plane"></i>
            </a>
            <?php //echo Yii::$app->name?>
        </div>
        <button class="navbar-toggler navbar-toggler-right text-uppercase font-weight-bold bg-primary text-white rounded" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            Меню
            <i class="fas fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item mx-0 mx-lg-1">
                    <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#portfolio">ПРОДУКЦИЯ</a>
                </li>
                <li class="nav-item mx-0 mx-lg-1">
                    <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#contact">Оформить заказ</a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<!-- Contact Section -->
<section class="masthead page-section" id="contact">
    <div class="container">

        <!-- Contact Section Heading -->
        <h2 class="page-section-heading text-center text-uppercase text-secondary mb-0">Оформить заказ</h2>

        <!-- Icon Divider -->
        <div class="divider-custom">
            <div class="divider-custom-line"></div>
            <div class="divider-custom-icon">
                <i class="fas fa-envelope"></i>
            </div>
            <div class="divider-custom-line"></div>
        </div>

        <!-- Contact Section Form -->
        <div class="row">
            <div class="col-lg-8 mx-auto">
                <!-- To configure the contact form email address, go to mail/contact_me.php and update the email address in the PHP file on line 19. -->
                <form name="sentMessage" id="contactForm" novalidate="novalidate">
                    <!--<div class="control-group">
                        <div class="form-group floating-label-form-group controls mb-0 pb-2">
                            <label>Имя</label>
                            <input class="form-control" id="name" type="text" placeholder="Имя" >
                        </div>
                        <p class="help-block text-danger"></p>
                    </div>
                    <div class="control-group">
                        <div class="form-group floating-label-form-group controls mb-0 pb-2">
                            <label>Телефон</label>
                            <input class="form-control" id="phone" type="tel" placeholder="Телефон" required="required">
                        </div>
                    </div>-->
                    <div class="control-group">
                        <div class="form-group">
                            <label for="name">Имя</label>
                            <input type="text" class="form-control form-control-lg" id="name" placeholder="Имя" required="required" data-validation-required-message="Пожалуйста, введите Ваше имя." required="required">
                            <p class="help-block text-danger"></p>
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="form-group">
                            <label for="phone">Телефон</label>
                            <input type="tel" class="form-control form-control-lg" id="phone" placeholder="Телефон" data-validation-required-message="Пожалуйста, введите свой номер телефона." required="required">
                            <p class="help-block text-danger"></p>
                        </div>

                    </div>
                    <br>
                    <div id="success"></div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-xl" id="sendMessageButton">Отправить</button>
                    </div>
                </form>
            </div>
        </div>

    </div>
</section>
<!-- Masthead -->
<header class="masthead bg-primary text-white text-center">
    <div class="container d-flex align-items-center flex-column">

        <!-- Masthead Avatar Image -->
        <img class="masthead-avatar mb-5" src="img/bk.png" alt="">

        <!-- Masthead Heading -->
        <h1 class="masthead-heading text-uppercase mb-0">Бальзамы Короткова</h1>

        <!-- Icon Divider -->
        <div class="divider-custom divider-light">
            <div class="divider-custom-line"></div>
            <div class="divider-custom-icon">
                <i class="fas fa-star"></i>
            </div>
            <div class="divider-custom-line"></div>
        </div>

        <!-- Masthead Subheading -->
        <p class="masthead-subheading font-weight-light mb-0">Не здоров!? Всего три слова: Пей Бальзамы Короткова!!!</p>

    </div>
</header>

<!-- Portfolio Section -->
<section class="page-section portfolio" id="portfolio">
    <div class="container">

        <!-- Portfolio Section Heading -->
        <h2 class="page-section-heading text-center text-uppercase text-secondary mb-0">ПРОДУКЦИЯ</h2>

        <!-- Icon Divider -->
        <div class="divider-custom">
            <div class="divider-custom-line"></div>
            <div class="divider-custom-icon">
                <i class="fas fa-star"></i>
            </div>
            <div class="divider-custom-line"></div>
        </div>

        <!-- Portfolio Grid Items -->
        <div class="row">
            <?php $cntProduct = 0; ?>
            <?php foreach ($products as $product): ?>
                <!-- Portfolio Item <?=++$cntProduct;?> -->
                <div class="col-md-6 col-lg-4">
                    <div class="portfolio-item mx-auto" data-toggle="modal" data-target="#portfolioModal<?=$cntProduct?>">
                        <!--<div class="portfolio-item-caption d-flex align-items-center justify-content-center h-100 w-100">
                            <div class="portfolio-item-caption-content text-center text-white">
                                <i class="fas fa-plus fa-3x"></i>
                            </div>
                        </div>-->
                        <div class="img-container">
                            <img class="img-fluid" src="img/products/<?=$product['photo'];?>" alt="">
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
        <!-- /.row -->

    </div>
</section>

<!-- About Section -->
<?php $this->render('_about'); ?>


<!-- Footer -->
<footer class="footer text-center">
    <div class="container">
        <div class="row">

            <!-- Footer Location -->
            <!--<div class="col-lg-4 mb-5 mb-lg-0">
                <h4 class="text-uppercase mb-4">Адрес</h4>
                <p class="lead mb-0">2215 John Daniel Drive
                    <br>Clark, MO 65243</p>
            </div>-->

            <!-- Footer Social Icons -->
            <div class="col-lg-12 mb-5 mb-lg-0">
                <h4 class="text-uppercase mb-4">Задайте свой вопрос</h4>
                <a class="btn btn-social mx-1" href="https://wa.me/<?=Yii::$app->params['whatsapp'];?>" target="_blank">
                    <i class="fab fa-7x fa-whatsapp"></i>
                </a>
                <a class="btn btn-primary btn-social mx-1" href="https://t.me/<?=Yii::$app->params['telegram'];?>" target="_blank">
                    <i class="fab fa-fw fa-telegram-plane"></i>
                </a>
            </div>



        </div>
    </div>
</footer>

<!-- Copyright Section -->
<section class="copyright py-4 text-center text-white">
    <div class="container">
        <small>Copyright &copy; <?=$_SERVER['HTTP_HOST'] . ' ' . date('Y')?></small>
    </div>
</section>

<!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
<div class="scroll-to-top d-lg-none position-fixed ">
    <a class="js-scroll-trigger d-block text-center text-white rounded" href="#page-top">
        <i class="fa fa-chevron-up"></i>
    </a>
</div>

<!-- Portfolio Modals -->
<?php $cntProduct = 0; ?>
<?php foreach ($products as $product): ?>
    <!-- Portfolio Modal <?=++$cntProduct;?> -->
    <div class="portfolio-modal modal fade" id="portfolioModal<?=$cntProduct;?>" tabindex="-1" role="dialog" aria-labelledby="portfolioModal1Label" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">
                <i class="fas fa-times"></i>
              </span>
                </button>
                <div class="modal-body">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-8">
                                <!-- Portfolio Modal - Title -->
                                <h2 class="portfolio-modal-title text-secondary text-uppercase mb-0 text-center"><?=$product['name']?>: <?=$product['slogan'];?></h2>
                                <!-- Icon Divider -->
                                <div class="divider-custom">
                                    <div class="divider-custom-line"></div>
                                    <div class="divider-custom-icon">
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <div class="divider-custom-line"></div>
                                </div>
                                <!-- Portfolio Modal - Image -->
                                <img class="img-fluid rounded mb-5" src="img/products/<?=$product['photo'];?>" alt="">
                                <!-- Portfolio Modal - Text -->
                                <div class="mb-5">
                                    <?=$product['description'];?>
                                </div>
                                <button class="btn btn-primary" href="#" data-dismiss="modal">
                                    <i class="fas fa-times fa-fw"></i>
                                    Закрыть
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endforeach; ?>

<!-- Pulse Phone Fixed Button -->
<a class="pulse" href="tel:<?=Yii::$app->params['phone']?>">
    <i class="fa fa-phone"></i>
</a>

<!-- Contact Fixed Button -->
<a class="btn btn-sm rounded js-scroll-trigger" id="contact_button" href="#contact">
    Оформить заказ
</a>