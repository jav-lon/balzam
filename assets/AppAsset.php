<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        /** Custom fonts for this theme */
        'vendor/fontawesome-free/css/all.min.css',
        'https://fonts.googleapis.com/css?family=Montserrat:400,700',
        'https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic',

        /** Theme CSS */
        'css/freelancer.min.css',
        /** Pulse Phone Button CSS */
        'css/fixed-buttons.css',
    ];
    public $js = [
        /** Bootstrap core JavaScript **/
        'vendor/jquery/jquery.min.js',
        'vendor/bootstrap/js/bootstrap.bundle.min.js',

        /** Plugin JavaScript */
        'vendor/jquery-easing/jquery.easing.min.js',

        /** Contact Form JavaScript */
        'js/jqBootstrapValidation.js',
        'js/contact_me.js',

        /** Custom scripts for this template */
        'js/freelancer.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
